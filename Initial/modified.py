"""
Reaction path diagram example with extensive dot code manipulations
Author: Tilman Bremer
Date: June 30th 2017
 
Written for Python 3.6 with cantera 2.3.0 but might as well work with other versions.
"""
import os
import cantera as ct
 
# Define a gas mixture at a high temperature that will undergo a reaction:
gas = ct.Solution('gri30.cti')
gas.TPX = 1500, 100000, 'CH4:0.07,O2:0.21,N2:0.79'
 
# Define a reactor, let it react until the temperature reaches 1800 K:
r = ct.IdealGasReactor(gas)
net = ct.ReactorNet([r])
T = r.T
while T < 1950:
	net.step()
	T = r.T
 
# Define the element to follow in the reaction path diagram:
element = 'N'
 
# Initiate the reaction path diagram:
diagram = ct.ReactionPathDiagram(gas, element)

# Options for cantera:
#diagram.scale = -1
diagram.show_details = False
diagram.font='CMU Serif Roman'
#diagram.threshold=0.01
diagram.dot_options='node[fontsize=15,shape="circle"]'
diagram.title = 'Reaction path diagram following {0}'.format(element)
  
# Define the filenames:
dot_file = 'ReactionPathDiagram.dot'
modified_dot_file = 'ReactionPathDiagramModified.dot'
img_file = 'ReactionPathDiagram'
 
# Write the dot-file first, then create the image from the dot-file with customizable
# parameters:
diagram.write_dot(dot_file)
 



# Let's open the just created dot file and make so adjustments before generating the image
# The dot_file is opened and read, the adjustements are saved in the modified_dot_file:
with open(modified_dot_file, 'wt') as outfile:
	with open(dot_file, 'rt') as infile:
		for row in infile:
			# Remove the line with the label:
			#if row.startswith(' label'):
			#	row = ""
			# Change lightness to zero, erase style command and replace setlinewidth with penwidth:
			row = row.replace(', 0.9"', ', 0.0"')
			row = row.replace('style="setlinewidth(', 'penwidth=')
			row = row.replace(')", arrowsize', ', arrowsize')
			# Find the lines with a color statement:
			if row.find('color="0.7, ') != -1:
				# Find the position of the saturation value:
				start=row.find('color="0.7, ')
				end=row.find(', 0.0"')
				saturation=float(row[start+12:end])
				if saturation > 1:
					# The highest values are set to 0 (black):
					saturationnew = 0
				else:
					# All values between 0 and 1 are inverted:
					saturationnew=round(abs(saturation-1),2)
					# Switch positions between saturation and lightness:
					row = row.replace(', 0.0"', '"')
					row = row.replace('0.7, ', '0.7, 0.0, ')
					# Replace saturation value with new calculated one:
				try:
					row = row.replace(str(saturation), str(saturationnew))
				except NameError:
					pass
			# Write the adjusted row:
			outfile.write(row)
# Call the command line and generate the image:
os.system('dot {0} -Tpng -Gdpi=300 -Nshape="ellipse" -O -o{1}'.format(modified_dot_file, img_file))
