"""
Viewing a reaction path diagram.

This script uses Graphviz to generate an image. You must have Graphviz installed
and the program 'dot' must be on your path for this example to work.
Graphviz can be obtained from https://www.graphviz.org/ or (possibly) installed
using your operating system's package manager.

Requires: cantera >= 2.5.0
"""

import os
import cantera as ct

# these lines can be replaced by any commands that generate
# an object of a class derived from class Kinetics in some state.
gas = ct.Solution('cs1.cti')

gastemp = 780.0
gaspres = 75.0
phi = 1.0
oxidizer = 'O2:1.0, N2:3.76'
fuel = 'NH3:1.0, NC7H16:0'
gas.TP = gastemp, gaspres*100000
gas.set_equivalence_ratio(phi, fuel, oxidizer)

r = ct.IdealGasReactor(gas)
net = ct.ReactorNet([r])
T = r.T
while T < 2400:
    net.step()
    T = r.T

element = 'N'

diagram = ct.ReactionPathDiagram(gas, element)

# Options for cantera:
diagram.scale = 1
diagram.show_details = False
diagram.font='CMU Serif Roman'
diagram.threshold=10000
diagram.dot_options='node[fontsize=20,shape="ellipse"]'
diagram.title = 'Reaction path diagram following {0}'.format(element)

  
# Define the filenames:
dot_file = 'ReactionPathDiagram.dot'
img_file = 'ReactionPathDiagram'
 
# Write the dot-file first, then create the image from the dot-file with customizable
# parameters:
diagram.write_dot(dot_file)
 
# Call the command line and generate the image:
os.system('dot {0} -Tpng -Gdpi=300 -Nshape="ellipse" -O -o{1}'.format(dot_file, img_file))
